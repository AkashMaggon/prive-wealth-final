$(document).ready(function() {
  $(".categoryLi").click(function() {
    $("#posts-list").hide();
    $("#posts-list-dynamic").show();
    console.log("clicked");
    $.ajax({
      url: "/forum/searchForum" + "?category=" + $(this).attr("category"),
      success: function(res) {
        console.log(res);
        createHTML({ forumsDynamic: res });
      },
      error: function(error) {
        console.log(error);
      }
    });
  });
});

function createHTML(storeData) {
  var compiledTemplate = Handlebars.compile(rawTemplate);
  var ourGeneratedHTML = compiledTemplate(storeData);
  var cmtsContainer = document.getElementById("storeLc");
  cmtsContainer.innerHTML = ourGeneratedHTML;
}
$("#posts-list-dynamic").hide();
var rawTemplate = `{{#each forumsDynamic}}
<article class="post-link">
<a href={{this.forumUrl}}>
 <figure class="post-thumbnail">
   <img src={{this.image}} alt="%POST_TITLE% Thumbnail" />
 </figure>

 <header class="post-header">
   <div class="post-meta">
     <time datetime=">2017-11-30">{{this.Date}}</time>
   </div>
   <h3>{{this.title}}</h3>
 </header>
</a>

<div class="post-excerpt">
 <p>
   {{this.subtitle}}
 </p>
</div>

<span class="post-categories">
 <span class="category"><a href="javascript:void(0);">Insights</a></span>
</span>
</article>
{{/each}}`;
