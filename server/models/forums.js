const mongoose = require("mongoose");

var Schema = mongoose.Schema;
const forumSchema = new Schema({
  forumAuthor: { type: String, default: null },
  Date: { type: String, default: null },
  image: { type: String, default: null },
  title: { type: String, default: null },
  category: { type: String, default: null },
  subtitle: { type: String, default: null },
  forumUrl: { type: String, default: null },
  body: { type: String, default: null }
});

module.exports = mongoose.model("Forum", forumSchema);
