const mongoose = require("mongoose");
const Forum = require("../models/forums");

function getForums(req, res, next) {
  Forum.find().then(docs => {
    console.log(docs);
    res.render("forums.hbs", { docs: docs });
  });
}

function getForum(req, res, next) {
  if (req.query.id) {
    Forum.find({ _id: req.query.id }).then(forum => {
      res.render("forumdetailtemplate.hbs", { forum: forum });
    });
  }
}

function createForum(req, res, next) {
  var forumAuthor = req.body.forumAuthor;
  var Date = req.body.Date;
  var image = req.body.image;
  var title = req.body.title;
  var category = req.body.category;
  var subtitle = req.body.subtitle;
  var body = req.body.forumBody;
  var forum = new Forum({
    forumAuthor: forumAuthor,
    Date: Date,
    image: image,
    title: title,
    category: category,
    subtitle: subtitle,
    body: body
  });

  forum.save().then(doc => {
    console.log(doc);
    doc.forumUrl = "/forum/forumDetails?id=" + doc._id;
    doc.save();
    res.send("Forum Successfully Saved!");
  });
}

function searchForum(req, res, next) {
  Forum.find({ category: req.query.category }).then(docs => {
    res.send(docs);
  });
}

module.exports = { getForums, getForum, createForum, searchForum };
