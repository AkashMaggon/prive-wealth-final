var express = require("express");
const router = express.Router();
var forumCtrl = require("../controllers/forums");

router.route("/").get(forumCtrl.getForums);

router.route("/forumDetails").get(forumCtrl.getForum);

router.route("/createForum").post(forumCtrl.createForum);

router.route("/searchForum").get(forumCtrl.searchForum);

module.exports = router;
