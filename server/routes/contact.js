var express = require("express");
const router = express.Router();
var contactCtrl = require("../controllers/contact");

router.route("/").post(contactCtrl.contactUs);

//router.route('/createForum').get(forumCtrl.createForum);

module.exports = router;
