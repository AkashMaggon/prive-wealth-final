var forum = require("./forum");
var contact = require("./contact");
var express = require("express");
const router = express.Router();
const hbs = require("hbs");

router.get("/health-check", (req, res) => {
  res.send("OK");
});

router.get("/", (req, res) => {
  res.render("index.hbs");
});

router.get("/privacy-policy", (req, res) => {
  res.render("privacy-policy.hbs");
});

router.use("/forum", forum);

router.use("/contact", contact);

module.exports = router;
