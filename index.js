var express = require("express");
var mongoose = require("mongoose");

var app = require("./config/express");

mongoose.connect("mongodb://localhost/prive-wealth");

 var db = mongoose.connection;


db.on("error", function(error) {
   console.log("connection error:", err);
 });


 db.once("open", function() {
   console.log("database connection successful");
 });

app.listen(80, () => {
  console.log("PriveWealth has started");
});


module.exports = app
